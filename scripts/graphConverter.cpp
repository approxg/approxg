#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <numeric>
#include <algorithm>
using namespace std;

#include <omp.h>

inline bool nextSNAPline(ifstream &infile, string &line, istringstream &iss, unsigned &src, unsigned &dest) {
	do {
		if(!getline(infile, line))
			return false;
	} while(line.length() == 0 || line[0] == '#');
	iss.clear();
	iss.str(line);
	return !!(iss >> src >> dest);
}

inline void getID(vector<unsigned> &idMap, unsigned &id, unsigned &nextID) {
	if(idMap.size() <= id) {
		idMap.resize(id + 2048, 0xffffffff);
	}
	if(idMap[id] == 0xffffffff) {
		idMap[id] = nextID;
		nextID++;
	}
	id = idMap[id];
}

vector<vector<unsigned> > loadSNAPlist(string fname) {
  srand(time(0));
	ifstream infile(fname.c_str());
	if(!infile) {
		cout << "File not available\n";
		throw 1;
	}
	unsigned nextID = 0;
	vector<unsigned> idMap;
	idMap.reserve(2048);
	vector<vector<unsigned> > g;
	string line;
	istringstream iss;
	unsigned src, dest;
  size_t lineNum = 0;
	while(nextSNAPline(infile, line, iss, src, dest)) {
    if(++lineNum % 1000000 == 0) {
      cout << "\r" << lineNum << " edges read";
      cout.flush();
    }
    if(src == dest) continue;
		getID(idMap, src, nextID);
		getID(idMap, dest, nextID);
		if(g.size() <= max(src, dest))
			g.resize(max(src, dest) + 1);
		g[src].push_back(dest);
    g[dest].push_back(src);
	}
	infile.close();
  cout << "\r                              \r";
  cout.flush();
	return g;
}

void randomize(vector<vector<unsigned> > &g) {
	vector<unsigned> idMap(g.size());
	iota(idMap.begin(), idMap.end(), 0);
	random_shuffle(idMap.begin(), idMap.end());
	vector<vector<unsigned> > gp(g.size());
  omp_lock_t outputLock;
  omp_init_lock(&outputLock);
  #pragma omp parallel for schedule(dynamic,64)
	for(long i = 0; i < g.size(); i++) {
    if(i % 1000 == 0) {
      omp_set_lock(&outputLock);
      cout << "\r" << i << " lists randomized         ";
      cout.flush();
      omp_unset_lock(&outputLock);
    }
		unsigned newid = idMap[i];
    gp[newid].reserve(g[i].size());
		for(const auto& dest : g[i]) {
			gp[newid].push_back(idMap[dest]);
		}
    g[i].resize(0);
	}
  omp_destroy_lock(&outputLock);
  cout << "\rMoving graph copy                 \r";
  cout.flush();
  g.clear();
	g = gp;
}

void outputAdj(string fname, vector<vector<unsigned> > &g) {
	ofstream outfile(fname.c_str());
	if(!outfile) {
		cerr << "File not opened\n";
		throw 1;
	}
	size_t edgeCount = 0;
  omp_lock_t countLock, outputLock;
  omp_init_lock(&countLock);
  omp_init_lock(&outputLock);
  #pragma omp parallel for schedule(dynamic,64)
	for(long long i = 0; i < g.size(); i++) {
    if(i % 1000 == 0) {
      omp_set_lock(&outputLock);
      cout << "\r" << i << " lists set-ified          ";
      cout.flush();
      omp_unset_lock(&outputLock);
    }
    sort(g[i].begin(), g[i].end());
    g[i].erase(unique(g[i].begin(), g[i].end()), g[i].end());
    omp_set_lock(&countLock);
		edgeCount += g[i].size();
    omp_unset_lock(&countLock);
	}
  omp_destroy_lock(&countLock);
	outfile << g.size() << ' ' << edgeCount << '\n';
	for(size_t i = 0; i < g.size(); i++) {
    if(i % 10000 == 0) {
      cout << "\r" << i << " lists outputted          ";
      cout.flush();
    }
		outfile << i << ' ' << g[i].size();
		for(const auto& dest : g[i]) {
			outfile << ' ' << dest;
		}
		outfile << '\n';
	}
  cout << "\r                              \r";
  cout.flush();
	outfile.close();
}

int main(int argc, char** argv) {
  if(argc != 2) {
    cerr << "usage: ./convert <dir>\n";
    return 1;
  }
  string dirname = argv[1];
	omp_set_dynamic(1);
  //omp_set_num_threads(omp_get_max_threads());
  cout << "load                            \n";
	auto g = loadSNAPlist(dirname + "/snap.txt");
  cout << "randomize                       \n";
	randomize(g);
  cout << "output ungraph                  \n";
	outputAdj(dirname + "/ungraph", g);
  cout << "done converting                 \n";
	return 0;
}
