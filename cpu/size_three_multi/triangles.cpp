#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <list>
//#include <random>
using namespace std;

#include <omp.h>
#include <mpi.h>

#include "graph.h"
#include "stats.h"

#define VERBOSE false
#define COST_AWARE true

int worldSize, worldRank;

enum Pattern {
  TRIANGLE,
  WEDGE
};
const int numPatterns = 2;
const int multiplicity[numPatterns] = { 3, 2 };

/*
 * finds size of intersection of sets stored as sorted vectors
 */
inline void countTris(const vector<unsigned> &Nu, const vector<unsigned> &Nv) {
  unsigned long long triCount = 0;
  vector<unsigned>::const_iterator it1 = Nu.begin(), it2 = Nv.begin();
  while(it1 != Nu.end() && it2 != Nv.end()) {
    if(*it1 < *it2) {
      it1++;
    } else if(*it1 > *it2) {
      it2++;
    } else {
      it1++;
      it2++;
      triCount++;
    }
  }
  unsigned long long wedgeCount = Nu.size() + Nv.size() - (2 * triCount) - 2;
  stats::accumulate_online(TRIANGLE, triCount);
  stats::accumulate_online(WEDGE, wedgeCount);
}

pair<double, double> pslr_calc(double p) {
  if(!COST_AWARE) return make_pair(p, p);
  const double frac_l = 1.0 / worldSize;
  const double frac_r = (worldSize - 1.0) / worldSize;
  if(p <= frac_l) {
    return make_pair(p * worldSize, 0.0);
  } else {
    return make_pair(1.0, (p - frac_l) / frac_r);
  }
}

inline void process_sample(Graph &g, double p, double p_previous, vector<pair<omp_lock_t, list<unsigned> > > &remoteRequirements) {
  if(!worldRank) printf("p=%f\n", p);
  pair<double, double> pslr = pslr_calc(p);
  pair<double, double> pslr_previous = pslr_calc(p_previous);
  size_t processing_edges = g.local_uni_edges();
  #pragma omp parallel for schedule(dynamic,64)
  for(size_t id = 0; id < processing_edges; id++) {
    pair<unsigned, unsigned> edge = g.edge(id);
    unsigned u = edge.first;
    unsigned v = edge.second;
    double r = g.randVal(id);
    if(g.isRemote(v)) {
      if((r < pslr.second) && (r >= pslr_previous.second)) {
        omp_set_lock(&(remoteRequirements[v].first));
        remoteRequirements[v].second.push_back(u);
        omp_unset_lock(&(remoteRequirements[v].first));
      }
    } else if((r < pslr.first) && (r >= pslr_previous.first)) {
      countTris(g.adjList(u), g.adjList(v));
    }
  }
  vector<unsigned> targets;
  targets.reserve(g.N());
  for(unsigned v = 0; v < g.N(); v++) {
    pair< omp_lock_t, list<unsigned> > p = remoteRequirements[v];
    if(!p.second.empty()) {
      targets.push_back(v);
    }
  }
  vector< vector<unsigned> > lists = g.requireData(targets);

  //remote computations
  #pragma omp parallel for schedule(dynamic,64)
  for(long v = 0; v < (long)g.N(); v++) {
    list<unsigned> &s = remoteRequirements[v].second;
    vector<unsigned> adj = lists[v];
    while(s.size() > 0) {
      unsigned loc = s.front();
      s.pop_front();
      countTris(adj, g.adjList(loc));
    }
  }
}

/*
 * Results from each thread are collected at index tid of a vector
 * Sum of these accumulated values is the triangle count
 * p_initial is a sampling ratio
 */
void graphlet_decomposition(Graph &g, const double p_initial, const double error_target, const double confidence) {
  stats::reset_online(numPatterns);
  
  vector<pair<omp_lock_t, list<unsigned> > > remoteRequirements(g.N());
  for(unsigned i = 0; i < g.N(); i++) {
    omp_init_lock(&(remoteRequirements[i].first));
  }
  double p = p_initial;
  process_sample(g, p, 0.0, remoteRequirements);
  double p_previous = p;

  unsigned long long patternEstimate[numPatterns];
  double CI[numPatterns], errorFrac[numPatterns];
  double maxErrorFrac = 0.0;
  for(int i = 0; i < numPatterns; i++) {
    patternEstimate[i] = stats::sum_online(i) / p / multiplicity[i];
    CI[i] = sqrt(stats::variance_online(i, g.M() / 2)) * stats::confidence_to_z(confidence) / multiplicity[i];
    errorFrac[i] = CI[i] / patternEstimate[i];
    if(errorFrac[i] > maxErrorFrac) {
      maxErrorFrac = errorFrac[i];
    }
  }
  if(0 == worldRank) {
    printf("triangles: %llu +- %f (%f%% error)\n", patternEstimate[TRIANGLE], CI[TRIANGLE], errorFrac[TRIANGLE] * 100);
    printf("wedges:    %llu +- %f (%f%% error)\n", patternEstimate[WEDGE], CI[WEDGE], errorFrac[WEDGE] * 100);
  }
  

  while(maxErrorFrac > error_target) { //Phase N
    double numerator = maxErrorFrac * maxErrorFrac * p_previous;
    double denominator = numerator + error_target * error_target * (1 - p_previous);
    p = numerator / denominator;
    if(p - p_previous < 0.0001) {
      p = p_previous + 0.0001;
    }
    process_sample(g, p, p_previous, remoteRequirements);
    p_previous = p;
    maxErrorFrac = 0.0;
    for(int i = 0; i < numPatterns; i++) {
      patternEstimate[i] = stats::sum_online(i) / p / multiplicity[i];
      CI[i] = sqrt(stats::variance_online(i, g.M() / 2)) * stats::confidence_to_z(confidence) / multiplicity[i];
      errorFrac[i] = CI[i] / patternEstimate[i];
      if(errorFrac[i] > maxErrorFrac) {
        maxErrorFrac = errorFrac[i];
      }
    }
    if(0 == worldRank) {
      printf("triangles: %llu +- %f (%f%% error)\n", patternEstimate[TRIANGLE], CI[TRIANGLE], errorFrac[TRIANGLE] * 100);
      printf("wedges:    %llu +- %f (%f%% error)\n", patternEstimate[WEDGE], CI[WEDGE], errorFrac[WEDGE] * 100);
    }
  }
  for(unsigned i = 0; i < g.N(); i++) {
    omp_destroy_lock(&(remoteRequirements[i].first));
  }
}

int main(int argc, char** argv) {
  int thread_level;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &thread_level);
  if(thread_level < MPI_THREAD_MULTIPLE) {
    fprintf(stderr, "Insufficient threading level\n");
    MPI_Abort(MPI::COMM_WORLD, 1);
  }
  omp_set_num_threads(omp_get_max_threads());
  worldSize = MPI::COMM_WORLD.Get_size();
  worldRank = MPI::COMM_WORLD.Get_rank();

  bool goodargs = (argc >= 2);
  string fname(argc >= 2 ? argv[1] : "");
  double p = (argc >= 3 ? strtod(argv[2], NULL) : 0.01);
  goodargs &= (p <= 1 && p > 0);
  double error_target = (argc >= 4 ? strtod(argv[3], NULL) : 0.01);
  goodargs &= (error_target <= 1 && error_target >= 0);
  double confidence = (argc >= 5 ? strtod(argv[4], NULL) : 0.95);
  goodargs &= (confidence < 1 && confidence > 0);
  if(!goodargs) {
    fprintf(stderr, "usage: ./count <fname> <sampling ratio (0,1]> <error target [0,1]> <confidence (0,1)>\n");
    return 1;
  }

  if(VERBOSE && worldRank == 0) {
    printf("processes: %d\n", worldSize);
    printf("threads:   %d\n", omp_get_max_threads());
  }
  Graph g(fname, worldSize, worldRank, VERBOSE);
  double t_start = MPI_Wtime();
  graphlet_decomposition(g, p, error_target, confidence);
  double t_end = MPI_Wtime();
  if(0 == worldRank) {
    printf("Wall time: %fs\n\n", (t_end-t_start));
  }
  MPI::Finalize();
  return 0;
}


