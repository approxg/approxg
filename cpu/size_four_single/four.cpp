#include <cstdio>
#include <vector>
#include <string>
#include <chrono>
#include <cstring>
#include <cstdlib>
using namespace std;

#include <omp.h>
#include "stats.h"
#include "mt19937.h"

vector<vector<unsigned> > graph;
vector<pair<unsigned, unsigned> > edges;
vector<double> randVals;
unsigned max_degree = 0;

enum Pattern {
  TRIANGLE,
  WEDGE,
  CLIQUE4,
  CYCLE4
};
const int multiplicity[] = { 3, 2, 6, 4 };

inline bool dropHalf(int a, int b) {
  if(a < b) return (a % 2) != 0;
  else return (b % 2) == 0;
}

inline unsigned long nextVal(FILE* fd, char* buf, size_t buflen, char* &pos) {
  size_t endOffset = buf + buflen - pos;
  if(endOffset < 32 && !feof(fd)) {
    memcpy(buf, pos, endOffset);
    size_t read_bytes = fread(buf+endOffset, 1, buflen-endOffset, fd);
    if(read_bytes + endOffset < buflen) {
      buf[read_bytes + endOffset] = '\0';
    }
    pos = buf;
  }
  char* newpos;
  unsigned long val = strtoul(pos, &newpos, 10);
  pos = newpos;
  return val;
}

/*
 * Loads adjacency list file into graph and edges arrays
 */
void graphFromAdj(string fname) {
  mt19937 rand_gen;
  graph.clear();
  edges.clear();
  
  const size_t buflen = (1 << 20);
  FILE* fd = fopen(fname.c_str(), "r");
  char* buf = new char[buflen];
  int bufferOffset = 0;

  char* pos = buf + buflen;

  unsigned N, src, deg, dest;
  size_t M;
  N = nextVal(fd, buf, buflen, pos);
  M = nextVal(fd, buf, buflen, pos);
  graph.resize(N);
  edges.reserve(M / 2);
  randVals.reserve(M / 2);
  for(unsigned i = 0; i < N; i++) {
    src = nextVal(fd, buf, buflen, pos);
    deg = nextVal(fd, buf, buflen, pos);
    graph[src].reserve(deg);
    if(deg > max_degree) max_degree = deg;
    for(; deg; deg--) {
      dest = nextVal(fd, buf, buflen, pos);
      graph[src].push_back(dest);
      if(dropHalf(src, dest)) {
        edges.push_back(make_pair(src, dest));
        randVals.push_back(rand_gen.real());
      }
    }
  }
  delete[] buf;
  fclose(fd);
  printf("N:     %d\nM:     %li\n", N, M / 2);
}

void markNeighbors(unsigned &v, unsigned &u, vector<char> &X) {
  for(unsigned w : graph[v]) {
    if(w != u) X[w] = 1;
  }
}

void clearNeighbors(unsigned &v, vector<char> &X) {
  for(unsigned w : graph[v]) {
    X[w] = 0;
  }
}

void triWedge(unsigned &v, unsigned &u, vector<unsigned> &Tri_e, unsigned long long &tri_count, vector<unsigned> &Star_u, unsigned long long &w_local_count, vector<char> &X) {
  for (unsigned w : graph[u]) {
    if(w == v)
      continue;
    if(X[w] == 1){
      X[w] = 3;
      Tri_e[tri_count] = w;
      tri_count++;
    }
    else {
      Star_u[w_local_count] = w;
      w_local_count++;
      X[w] = 2;
    }
  }
}

void cycleCount(unsigned long long &w_local_count, vector<unsigned> &Star_u, unsigned long long &cycle4_count, unsigned &v, vector<char> &X) {
  for(unsigned long long j = 0; j < w_local_count; j++) {
    unsigned w = Star_u[j];
    for (unsigned i = 0; i < graph[w].size(); i++) {
      if (X[graph[w][i]] == 1)
        cycle4_count++;
    }
    Star_u[j] = 0;
  }
}

void cliqueCount(unsigned long long &tri_count, vector<unsigned> &Tri_e, unsigned long long &clique4_count, unsigned &v, vector<char> &X) {
  for(unsigned long long tr_i = 0; tr_i < tri_count; tr_i++) {
    unsigned w = Tri_e[tr_i];
    for(unsigned i = 0; i < graph[w].size(); i++) {
      if(X[graph[w][i]] == 3)
        clique4_count++;
    }
    X[w] = 0;
    Tri_e[tr_i] = 0;
  }
}

void process_sample(double p, double p_previous) {
  const long long n = graph.size(), m = edges.size();
  vector<unsigned> T_vu(max_degree+1, 0), W_u(max_degree+1, 0);
  vector<char> X(n, 0);
  #pragma omp parallel for schedule(dynamic,64) firstprivate(X,T_vu,W_u)
  for(long long e = 0; e < m; e++) {
    if(p_previous < randVals[e] && randVals[e] <= p) {
      unsigned v = edges[e].first;
      unsigned u = edges[e].second;
      unsigned long long w_local_count = 0, tri_count = 0, clique4_count = 0, cycle4_count = 0;
      markNeighbors(v, u, X);
      triWedge(v, u, T_vu, tri_count, W_u, w_local_count, X);
      cycleCount(w_local_count, W_u, cycle4_count, v, X);
      cliqueCount(tri_count, T_vu, clique4_count, v, X);
      stats::accumulate_online(TRIANGLE, tri_count);
      stats::accumulate_online(WEDGE, w_local_count);
      stats::accumulate_online(CLIQUE4, clique4_count);
      stats::accumulate_online(CYCLE4, cycle4_count);
      clearNeighbors(v, X);
    }
  }
}

void graphlet_decomposition(double p_initial, double error_target, double confidence) {
  const long long m = edges.size();
  stats::reset_online(4);

  string patternName[] = { "triangles", "wedges ", "4-cliques", "4-cycles"};
  double p = p_initial, p_previous = 0.0;
  double maxErrorFrac = 0.0;
  do {
    printf("p=%f\n", p);
    process_sample(p, p_previous);
    p_previous = p;
    maxErrorFrac = 0.0;
    for(int i = 0; i < 4; i++) {
      unsigned long long patternEstimate = stats::sum_online(i) / p / multiplicity[i];
      double CI = sqrt(stats::variance_online(i, m)) * stats::confidence_to_z(confidence) / multiplicity[i];
      double errorFrac = CI / patternEstimate;
      if(errorFrac > maxErrorFrac) maxErrorFrac = errorFrac;
      printf("%s:\t%llu +- %f (%f%% error)\n", patternName[i].c_str(), patternEstimate, CI, errorFrac * 100);
    }
    double numerator = maxErrorFrac * maxErrorFrac * p_previous;
    double denominator = numerator + error_target * error_target * (1 - p_previous);
    p = numerator / denominator;
  } while(maxErrorFrac > error_target);
}

int main(int argc, char** argv) {
  bool goodargs = (argc >= 2);
  string fname(argc >= 2 ? argv[1] : "");
  double p = (argc >= 3 ? strtod(argv[2], NULL) : 0.01);
  goodargs &= (p <= 1 && p >= 0);
  double error_target = (argc >= 4 ? strtod(argv[3], NULL) : 0.01);
  goodargs &= (error_target <= 1 && error_target >= 0);
  double confidence = (argc >= 5 ? strtod(argv[4], NULL) : 0.95);
  goodargs &= (confidence <= 1 && confidence >= 0);
  if(!goodargs) {
    fprintf(stderr, "usage: ./count <fname> <sampling ratio [0,1]> <error target [0,1]> <confidence [0,1]>\n");
    return 1;
  }

  printf("Opening adj file: %s\n", fname.c_str());
  graphFromAdj(fname);
  printf("worker threads:  %d\n", omp_get_max_threads());
  omp_set_num_threads(omp_get_max_threads());

  auto t_start = chrono::high_resolution_clock::now();
  graphlet_decomposition(p, error_target, confidence);
  auto t_end = std::chrono::high_resolution_clock::now();
  printf("Wall time: %fs\n\n", chrono::duration<double, ratio<1,1>>(t_end-t_start).count());
  return 0;
}
