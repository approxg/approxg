#pragma once
#include <vector>
#include <fstream>
#include <string>
#include <mpi.h>
#include <omp.h>

#define REQ_TAG 6
#define ADJ_TAG 7

#define REQ_MUL_TAG 10
#define ADJ_MUL_TAG 12

#define DONE_TAG 25

class Graph {
private:
  const int nodeID, worldSize;
  bool verbose;
  unsigned startIndex, endIndex;
  size_t localEdges;
  unsigned max_deg;
  unsigned N_;
  size_t M_;
  std::vector< std::vector<unsigned> > nodeToAdjList;
  std::vector< std::pair<unsigned, unsigned> > edgeList;
  std::vector<double> randList;
  std::vector<unsigned> nodeDivisions; //max id in each division
  std::vector<unsigned> degree;
  std::vector<omp_lock_t> remoteLocks;
  bool safeToDefer;
  inline bool setDeferSafety(bool setting);
  void printInfo() const;
  static inline bool dropHalf(unsigned a, unsigned b) {
    if(a < b) return (a % 2) != 0;
    else return (b % 2) == 0;
  }
  MPI_Status deferUntil(MPI_Request untilRequest);
  
public:
  Graph(std::string fname, int divisions=1, int divIndex=0, bool verbose=false);

  inline std::vector<unsigned>& adjList(const unsigned id) {
    if(!isLocal(id)) {
      fprintf(stderr, "Invalid access to %d from process %d\n", id, nodeID);
      MPI_Abort(MPI::COMM_WORLD, MPI_ERR_OTHER);
    }
    return nodeToAdjList[id - startIndex];
  }
  inline std::pair<unsigned, unsigned>& edge(size_t id) {
    if(id >= edgeList.size()) {
      fprintf(stderr, "Invalid access to edge %lu from process %d\n", id, nodeID);
      MPI_Abort(MPI::COMM_WORLD, MPI_ERR_OTHER);
    }
    return edgeList[id];
  }
  inline double randVal(size_t id) {
    if(id >= edgeList.size()) {
      fprintf(stderr, "Invalid access to edge %lu from process %d\n", id, nodeID);
      MPI_Abort(MPI::COMM_WORLD, MPI_ERR_OTHER);
    }
    return randList[id];
  }
  
  inline int nodeForVertex(const unsigned id) const;
  inline bool isLocal(const unsigned id) const {
    return id >= startIndex && id <= endIndex;
  }
  inline bool isRemote(const unsigned id) const {
    return id < startIndex || id > endIndex;
  }

  unsigned N() const { return N_; }
  size_t M() const { return M_; }
  inline unsigned min_index() const { return startIndex; }
  inline unsigned max_index() const { return endIndex; }
  inline unsigned local_edges() const { return localEdges; }
  inline size_t local_uni_edges() const { return edgeList.size(); }
  inline unsigned deg(unsigned id) const { return degree[id]; }
  inline unsigned max_degree() const { return max_deg; }
  
  inline unsigned bufferSize(std::vector<unsigned> targets) {
    unsigned size = 0;
    for(std::vector<unsigned>::iterator it = targets.begin(); it != targets.end(); ++it) {
      unsigned id = *it;
      size += degree[id]; 
    }
    return size;
  }
  std::vector< std::vector<unsigned> > requireData(std::vector<unsigned> targets);

  std::vector<unsigned> remoteAdjList(unsigned id);
  bool defer();
  void barrier();
};

