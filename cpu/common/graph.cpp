#include "graph.h"
#include <cstdio>
#include <sstream>
#include <algorithm>

#include "mt19937.h"

#define DEBUG false

Graph::Graph(std::string fname, int divisions, int divIndex, bool v) : nodeID(divIndex), worldSize(divisions), verbose(v) {
  mt19937 rand_gen;
  nodeDivisions.resize(divisions, -1);
  remoteLocks.resize(worldSize);
  for(int i = 0; i < worldSize; i++) {
    omp_init_lock(&(remoteLocks[i]));
  }
  std::ifstream infile(fname.c_str());
  if(!infile.is_open()) {
    fprintf(stderr, "File %s unavailable\n", fname.c_str());
    MPI_Abort(MPI::COMM_WORLD, MPI_ERR_OTHER);
    return;
  }
  std::string line;
  max_deg = 0;
  N_ = M_ = 0;
  localEdges = 0;
  int currentDivision = 0;
  size_t divCount = 0, divTarget = 0;
  unsigned src, deg, dest;
  bool ok = true;
  startIndex = endIndex = 0;
  while(std::getline(infile, line) && ok) {
    std::istringstream iss(line);
    if(0 == N_) {
      ok &= static_cast<bool>(iss >> N_ >> M_);
      divTarget = (M_ - 1) / divisions + 1;
      degree.resize(N_, 0);
      nodeToAdjList.reserve(1.1 * N_ / divisions);
      edgeList.reserve(0.55 * M_ / divisions);
      randList.reserve(0.55 * M_ / divisions);
    } else {
      ok &= static_cast<bool>(iss >> src >> deg);
      divCount += deg;
      degree[src] = deg;
      if(deg > max_deg)
        max_deg = deg;
      if(currentDivision == divIndex) {
        if(0 == localEdges) {
          startIndex = src;
        }
        std::vector<unsigned> v;
        std::vector<double> vr;
        v.reserve(deg);
        vr.reserve(deg);
        for(unsigned i = 0; i < deg; i++) {
          ok &= static_cast<bool>(iss >> dest);
          v.push_back(dest);
          if(Graph::dropHalf(src, dest)) {
            edgeList.push_back(std::make_pair(src, dest));
            randList.push_back(rand_gen.real());
          }
        }
        nodeToAdjList.push_back(v);
        localEdges += deg;
        endIndex = src;
      }
      if(divCount >= divTarget) {
        nodeDivisions[currentDivision] = src;
        currentDivision++;
        divCount -= divTarget;
      }
    }
  }
  if(!ok) {
    fprintf(stderr, "Graph formatting error\n");
  }
  infile.close();
  nodeDivisions[currentDivision] = src;
  if(0 == endIndex)
    endIndex = src;
  if(verbose)
    printInfo();
  safeToDefer = true;
  MPI_Barrier(MPI::COMM_WORLD);
}

void Graph::printInfo() const {
  double pct_e = 100. * localEdges / M_;
  double pct_v = 100. * (endIndex - startIndex + 1) / N_;
  std::ostringstream out;
  out << "(graph.h) process " << nodeID << " got ";
  out << localEdges << "/" << M_ << " edges (";
  out << pct_e << "%)\n";
  out << "          vertex bounds: [";
  out << startIndex << ", " << endIndex << "] (";
  out << pct_v << "%)\n";
  fprintf(stdout, "%s", out.str().c_str());
}

/*
 * compute node on which graph vertex resides
 */
inline int Graph::nodeForVertex(const unsigned id) const {
  if(id >= startIndex && id <= endIndex)
    return nodeID;
  else 
    return std::lower_bound(nodeDivisions.begin(), nodeDivisions.end(), id) - nodeDivisions.begin();
}

std::vector<unsigned> Graph::remoteAdjList(unsigned id) {
  std::vector<unsigned> adj(degree[id]);
  int remote = nodeForVertex(id);
  omp_set_lock(&(remoteLocks[remote]));
  if(DEBUG && verbose) fprintf(stderr, "Rank %d making request to rank %d\n", nodeID, remote);
  MPI_Request request;
  MPI_Irecv(&(adj[0]), degree[id], MPI_UNSIGNED, remote, ADJ_TAG, MPI::COMM_WORLD, &request);
  MPI_Send(&id, 1, MPI_UNSIGNED, remote, REQ_TAG, MPI::COMM_WORLD);
  MPI_Status status;
  for(int flag = 0; flag == 0; MPI_Test(&request, &flag, &status)) {
    defer();
  }
  omp_unset_lock(&(remoteLocks[remote]));
  return adj;
}

std::vector< std::vector<unsigned> > Graph::requireData(std::vector<unsigned> targets) {
  std::vector< std::vector<unsigned> > machineTargets(worldSize);
  for(std::vector<unsigned>::iterator it = targets.begin(); it != targets.end(); ++it) { //find location of required data
    unsigned v = *it;
    machineTargets[nodeForVertex(v)].push_back(v);
  }
  std::vector<unsigned> outcounts(worldSize), incounts(worldSize, 0);
  for(int i = 0; i < worldSize; i++) {
    outcounts[i] = machineTargets[i].size();
  }
  //share number of requirements among pairs
  MPI_Alltoall(&(outcounts[0]), 1, MPI_UNSIGNED, &(incounts[0]), 1, MPI_UNSIGNED, MPI::COMM_WORLD);
  std::vector< std::vector<unsigned> > machineRequirements(worldSize);
  for(int i = 0; i < worldSize; i++) {
    machineRequirements[i].resize(incounts[i]);
  }
  std::vector<MPI_Request> requests(2 * (worldSize - 1));
  std::vector<MPI_Status> statuses(2 * (worldSize - 1));
  int requestIndex = 0;
  for(int i = 0; i < worldSize; i++) { //receive requirements
    if(i != nodeID) {
      MPI_Irecv(&(machineRequirements[i][0]), incounts[i], MPI_UNSIGNED, i, 15, MPI::COMM_WORLD, &(requests[requestIndex]));
      requestIndex++;
    }
  }
  for(int i = 0; i < worldSize; i++) { //send requirements
    if(i != nodeID) {
      MPI_Isend(&(machineTargets[i][0]), machineTargets[i].size(), MPI_UNSIGNED, i, 15, MPI::COMM_WORLD, &(requests[requestIndex]));
      requestIndex++;
    }
  }
  MPI_Waitall((2 * (worldSize - 1)), &(requests[0]), &(statuses[0]));
  requestIndex = 0;
  std::vector< std::vector<unsigned> > incomingData(worldSize);
  for(int i = 0; i < worldSize; i++) { //receive data
    if(i != nodeID) {
      unsigned len = bufferSize(machineTargets[i]);
      incomingData[i].resize(len);
      MPI_Irecv(&(incomingData[i][0]), len, MPI_UNSIGNED, i, 16, MPI::COMM_WORLD, &(requests[requestIndex]));
      requestIndex++;
    }
  }
  std::vector< std::vector<unsigned> > outgoingData(worldSize);
  for(int i = 0; i < worldSize; i++) { //send requested data
    if(i != nodeID) {
      unsigned len = bufferSize(machineRequirements[i]);
      outgoingData[i].reserve(len);
      for(unsigned j = 0; j < incounts[i]; j++) {
        std::vector<unsigned> adj = adjList(machineRequirements[i][j]);
        outgoingData[i].insert(outgoingData[i].end(), adj.begin(), adj.end());
      }
      MPI_Isend(&(outgoingData[i][0]), len, MPI_UNSIGNED, i, 16, MPI::COMM_WORLD, &(requests[requestIndex]));
      requestIndex++;
    }
  }
  MPI_Waitall((2 * (worldSize - 1)), &(requests[0]), &(statuses[0]));
  std::vector< std::vector<unsigned> > data(N_);
  for(int i = 0; i < worldSize; i++) { //send requested data
    if(i != nodeID) {
      unsigned start = 0;
      for(std::vector<unsigned>::iterator it = machineTargets[i].begin(); it != machineTargets[i].end(); ++it) {
        unsigned v = *it;
        unsigned d = degree[v];
        std::vector<unsigned> adj(incomingData[i].begin() + start, incomingData[i].begin() + start + d);
        data[v] = adj;
        start += d;
      }
    }
  }
  return data;
}

inline bool Graph::setDeferSafety(bool setting) {
  bool success;
  #pragma omp critical(defer)
  {
    if(false == setting) {
      success = safeToDefer;
      safeToDefer = false;
    } else {
      success = safeToDefer = true;
    }
  }
  return success;
}

MPI_Status Graph::deferUntil(MPI_Request untilRequest) {
  MPI_Status status;
  if(setDeferSafety(false)) {
    MPI_Request requests[2];
    requests[0] = untilRequest;
    int indx;
    unsigned target;
    MPI_Irecv(&target, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, REQ_TAG, MPI::COMM_WORLD, &(requests[1]));
    MPI_Waitany(2, requests, &indx, &status);
    while(0 != indx) {
      if(isRemote(target)) {
        fprintf(stderr, "Bad request for vertex %d to rank %d with id bounds [%d,%d]\n", target, nodeID, startIndex, endIndex);
        MPI_Abort(MPI::COMM_WORLD, MPI_ERR_OTHER);
      } else {
        int remote = status.MPI_SOURCE;
        std::vector<unsigned> adj = adjList(target);
        MPI_Rsend(&(adj[0]), adj.size(), MPI_UNSIGNED, remote, ADJ_TAG, MPI::COMM_WORLD);
        if(DEBUG && verbose) fprintf(stderr, "Rank %d served request from rank %d during deferUntil\n", nodeID, remote);
      }
      MPI_Irecv(&target, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, REQ_TAG, MPI::COMM_WORLD, &(requests[1]));
      MPI_Waitany(2, requests, &indx, &status);
    }
    MPI_Cancel(&requests[1]);
    setDeferSafety(true);
  } else {
    MPI_Wait(&untilRequest, &status);
  }
  return status;
}

/*
 * Handle one remote request
 */
bool Graph::defer() {
  int flag = false;
  if(setDeferSafety(false)) {
    unsigned target;
    MPI_Status status;
    MPI_Iprobe(MPI_ANY_SOURCE, REQ_TAG, MPI::COMM_WORLD, &flag, &status);
    if(flag) {
      MPI_Recv(&target, 1, MPI_UNSIGNED, MPI_ANY_SOURCE, REQ_TAG, MPI::COMM_WORLD, &status);
      if(isRemote(target)) {
        fprintf(stderr, "Bad request for vertex %d to rank %d with id bounds [%d,%d]\n", target, nodeID, startIndex, endIndex);
        MPI_Abort(MPI::COMM_WORLD, MPI_ERR_OTHER);
      } else {
        std::vector<unsigned> adj = adjList(target);
        MPI_Rsend(&(adj[0]), adj.size(), MPI_UNSIGNED, status.MPI_SOURCE, ADJ_TAG, MPI::COMM_WORLD);
        if(DEBUG && verbose) fprintf(stderr, "Rank %d served request from rank %d during defer\n", nodeID, status.MPI_SOURCE);
      }
    }
    setDeferSafety(true);
  }
  return flag;
}

/*
 * wrap MPI_Barrier to serve remote requests while waiting
 */
#if MPI_VERSION < 3
void Graph::barrier() {
  if(DEBUG && verbose) fprintf(stderr, "Rank %d at barrier\n", nodeID);
  int temp = 0;
  MPI_Request request;
  for(int i = 0; i < worldSize; i++) {
    if(i != nodeID) {
      MPI_Irecv(&temp, 1, MPI_INT, i, DONE_TAG, MPI::COMM_WORLD, &request);
      if(verbose) fprintf(stderr, "Rank %d waiting for rank %d \n", nodeID, i);
      deferUntil(request);
    } else {
      for(int j = 0; j < worldSize; j++) {
        if(j != nodeID) {
          MPI_Issend(&temp, 1, MPI_INT, j, DONE_TAG, MPI::COMM_WORLD, &request);
          if(verbose) fprintf(stderr, "Rank %d notifying rank %d \n", nodeID, j);
          deferUntil(request);
        }
      }
    }
  } 
}
#else
void Graph::barrier() {
  if(DEBUG && verbose) fprintf(stderr, "Rank %d at barrier\n", nodeID);
  MPI_Request request;
  MPI_Ibarrier(MPI::COMM_WORLD, &request);
  deferUntil(request);
}
#endif

