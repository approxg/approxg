#include <map>
using namespace std;

class options {
  private:
    map<string, string> m;
  public:
    void add(string k, string v) {
      m[k] = v;
    }
    bool has(string k) {
      return m.find(k) != m.end();
    }
    string sval(string k) {
      return has(k) ? m[k] : "";;
    }
    double dval(string k) {
      try {
        return has(k) ? stod(m[k]) : 0.0;
      } catch(...) {
        return 0.0;
      }
    }
    int ival(string k) {
      try {
        return has(k) ? stoi(m[k]) : 0;
      } catch(...) {
        return 0;
      }
    }
};

options getOptions(int a_argc, char** a_argv) {
  char** argv = a_argv + 1;
  int argc = a_argc - 1;
  options o;
  for(int i = 0; i < argc; i++) {
    string opt(argv[i]);
    int index = opt.find("=");
    string k = opt.substr(2, index-2);
    string v = opt.substr(index + 1);
    o.add(k, v);
  }
  return o;
}
