#include <cstdio>
#include <cmath>
#include <vector>
#include <algorithm>
using namespace std;

#include <omp.h>
#include <mpi.h>
#include "graph.h"
#include "stats.h"

#define VERBOSE false
#define COST_AWARE true

int worldSize, worldRank;
vector<char> sampled;

typedef vector< vector<unsigned> > ListContainer;

enum Pattern {
  TRIANGLE,
  WEDGE,
  CLIQUE4,
  //CCYCLE4,
  CYCLE4
  //PATH4
};
const int numPatterns = 4;
const int multiplicity[numPatterns] = { 3, 2, 6, 4 };
//const int multiplicity[numPatterns] = { 3, 2, 6, 1, 4, 1 };

inline void markNeighbors(vector<unsigned> &adj_v, unsigned &u, vector<char> &X) {
  for(unsigned i = 0; i < adj_v.size(); i++) {
    unsigned w = adj_v[i];
    if(w != u) X[w] = 1;
  }
}

inline void clearNeighbors(vector<unsigned> &adj_v, vector<char> &X) {
  for(unsigned i = 0; i < adj_v.size(); i++) {
    X[adj_v[i]] = 0;
  }
}

inline void triWedge(unsigned &v, vector<unsigned> &adj_u, vector<unsigned> &Tri_e, unsigned long long &tri_count, vector<unsigned> &Star_u, unsigned long long &w_local_count, vector<char> &X) {
  for(unsigned i = 0; i < adj_u.size(); i++) {
    unsigned w = adj_u[i];
    if(w == v)
      continue;
    if(X[w] == 1){
      X[w] = 3;
      Tri_e[tri_count] = w;
      tri_count++;
    }
    else {
      Star_u[w_local_count] = w;
      w_local_count++;
      X[w] = 2;
    }
  }
}

inline void cycleCount(Graph &g, unsigned long long &w_local_count, vector<unsigned> &Star_u, unsigned long long &cycle4_count, ListContainer &lists, vector<char> &X) {
  for(unsigned long long j = 0; j < w_local_count; j++) {
    unsigned w = Star_u[j];
    if(g.isLocal(w)) {
      vector<unsigned> adj_w = g.adjList(w);
      for(unsigned i = 0; i < adj_w.size(); i++) {
        unsigned y = adj_w[i];
        if(X[y] == 1) {
          cycle4_count++;
        }
      }
    } else if(!lists[w].empty()) {
      for(unsigned i = 0; i < lists[w].size(); i++) {
        unsigned y = lists[w][i];
        if(X[y] == 1) {
          cycle4_count++;
        }
      }
    } else {
      fprintf(stderr, "Looking for a missing list in map\n");
    }
    Star_u[j] = 0;
  }
}

inline void cliqueCount(Graph &g, unsigned long long &tri_count, vector<unsigned> &Tri_e, unsigned long long &clique4_count, ListContainer &lists, vector<char> &X) {
  for(unsigned long long tr_i = 0; tr_i < tri_count; tr_i++) {
    unsigned w = Tri_e[tr_i];
    if(g.isLocal(w)) {
      vector<unsigned> adj_w = g.adjList(w);
      for(unsigned i = 0; i < adj_w.size(); i++) {
        unsigned y = adj_w[i];
        if(X[y] == 3) {
          clique4_count++;
        }
      }
    } else if(!lists[w].empty()) {
      for(unsigned i = 0; i < lists[w].size(); i++) {
        unsigned y = lists[w][i];
        if(X[y] == 3) {
          clique4_count++;
        }
      }
    } else {
      fprintf(stderr, "Looking for a missing list in map\n");
    }
    X[w] = 0;
    Tri_e[tr_i] = 0;
  }
}

//u is local, v may be remote
inline void process_edge(Graph &g, unsigned &u, unsigned &v, vector<unsigned> &T_vu, vector<unsigned> &W_u, vector<char> &X, ListContainer &lists) {
  if(g.isLocal(v)) {
    markNeighbors(g.adjList(v), u, X);
  } else if(!lists[v].empty()) {
    markNeighbors(lists[v], u, X);
  } else {
    fprintf(stderr, "Looking for a missing list in map\n");
  }
  unsigned long long w_local_count = 0, tri_count = 0, clique4_count = 0, cycle4_count = 0;
  triWedge(v, g.adjList(u), T_vu, tri_count, W_u, w_local_count, X);
  cycleCount(g, w_local_count, W_u, cycle4_count, lists, X);
  cliqueCount(g, tri_count, T_vu, clique4_count, lists, X);
  if(g.isLocal(v)) {
    clearNeighbors(g.adjList(v), X);
  } else {
    clearNeighbors(lists[v], X);
  }
  stats::accumulate_online(TRIANGLE, tri_count);
  stats::accumulate_online(WEDGE, g.deg(u) + g.deg(v) - 2 * tri_count - 2);
  stats::accumulate_online(CLIQUE4, clique4_count);
  //stats::accumulate_online(CCYCLE4, tri_count * (tri_count - 1) / 2 - clique4_count);
  stats::accumulate_online(CYCLE4, cycle4_count);
  //stats::accumulate_online(PATH4, (g.deg(u) - tri_count - 1) * (g.deg(v) - tri_count - 1) - cycle4_count);
}

pair<double, double> pslr_calc(double p) {
  if(!COST_AWARE) return make_pair(p, p);
  const double frac_l = 1.0 / worldSize;
  const double frac_r = (worldSize - 1.0) / worldSize;
  if(p <= frac_l) {
    return make_pair(p * worldSize, 0.0);
  } else {
    return make_pair(1.0, (p - frac_l) / frac_r);
  }
}

inline void process_sample(Graph &g, double p, double p_previous) {
  if(!worldRank) printf("p=%f\n", p);
  pair<double, double> pslr = pslr_calc(p);
  pair<double, double> pslr_previous = pslr_calc(p_previous);
  size_t processing_edges = g.local_uni_edges();
  vector<char> processEdge(processing_edges, 0);
  #pragma omp parallel for
  for(size_t id = 0; id < processing_edges; id++) {
    if(0 == sampled[id]) {
      pair<double, double> edge = g.edge(id);
      double r = g.randVal(id);
      if(g.isLocal(edge.second)) {
        if((r < pslr.first) && (r >= pslr_previous.first)) {
          sampled[id] = 1;
          processEdge[id] = 1;
        }
      } else {
        if((r < pslr.second) && (r >= pslr_previous.second)) {
          sampled[id] = 1;
          processEdge[id] = 1;
        }
      }
    }
  }

  vector<bool> neighborsRequired(g.N(), false);
  vector<bool> isTarget(g.N(), false);
  vector<unsigned> targets;
  targets.reserve(g.N());
  for(size_t id = 0; id < processing_edges; id++) {
    unsigned u = g.edge(id).first;
    if((1 == processEdge[id]) && !neighborsRequired[u]) {
      neighborsRequired[u] = true;
      vector<unsigned> adj_u = g.adjList(u);
      for(unsigned i = 0; i < adj_u.size(); i++) {
        unsigned v = adj_u[i];
        if(g.isRemote(v) && !isTarget[v]) {
          isTarget[v] = true;
          targets.push_back(v);
        }
      }
    }
  }
  ListContainer lists = g.requireData(targets);

  vector<unsigned> T_vu(g.max_degree()+1,0), W_u(g.max_degree()+1,0);
  vector<char> X(g.N(), 0);
  #pragma omp parallel for firstprivate(X,T_vu,W_u)
  for(size_t id = 0; id < processing_edges; id++) {
    if(1 == processEdge[id]) {
      pair<unsigned, unsigned> edge = g.edge(id);
      process_edge(g, edge.first, edge.second, T_vu, W_u, X, lists);
    }
  }
}

void graphlet_decomposition(Graph g, const double p_initial, const double error_target, const double confidence) {
  stats::reset_online(numPatterns);
  sampled.assign(g.local_uni_edges(), 0);

  double p = p_initial;
  process_sample(g, p, 0.0);
  double p_previous = p;
  unsigned long long patternEstimate[numPatterns];
  double CI[numPatterns], errorFrac[numPatterns];
  double maxErrorFrac = 0.0;
  for(int i = 0; i < numPatterns; i++) {
    patternEstimate[i] = stats::sum_online(i) / p / multiplicity[i];
    CI[i] = sqrt(stats::variance_online(i, g.M() / 2)) * stats::confidence_to_z(confidence) / multiplicity[i];
    errorFrac[i] = CI[i] / patternEstimate[i];
    if(errorFrac[i] > maxErrorFrac) {
      maxErrorFrac = errorFrac[i];
    }
  }
  if(0 == worldRank) {
    printf("triangles:   %llu +- %f (%f%% error)\n", patternEstimate[TRIANGLE], CI[TRIANGLE], errorFrac[TRIANGLE] * 100);
    printf("wedges:      %llu +- %f (%f%% error)\n", patternEstimate[WEDGE], CI[WEDGE], errorFrac[WEDGE] * 100);
    printf("4-clique:    %llu +- %f (%f%% error)\n", patternEstimate[CLIQUE4], CI[CLIQUE4], errorFrac[CLIQUE4] * 100);
    //printf("4-chordal:   %llu +- %f (%f%% error)\n", patternEstimate[CCYCLE4], CI[CCYCLE4], errorFrac[CCYCLE4] * 100);
    printf("4-cycle:     %llu +- %f (%f%% error)\n", patternEstimate[CYCLE4], CI[CYCLE4], errorFrac[CYCLE4] * 100);
    //printf("4-path:      %llu +- %f (%f%% error)\n", patternEstimate[PATH4], CI[PATH4], errorFrac[PATH4] * 100);
  } 

  while(maxErrorFrac > error_target) {
    double numerator = maxErrorFrac * maxErrorFrac * p_previous;
    double denominator = numerator + error_target * error_target * (1 - p_previous);
    p = numerator / denominator;
    if(p - p_previous < 0.0001) {
      p = p_previous + 0.0001;
    }
    process_sample(g, p, p_previous);
    p_previous = p;

    maxErrorFrac = 0.0;
    for(int i = 0; i < numPatterns; i++) {
      patternEstimate[i] = stats::sum_online(i) / p / multiplicity[i];
      CI[i] = sqrt(stats::variance_online(i, g.M() / 2)) * stats::confidence_to_z(confidence) / multiplicity[i];
      errorFrac[i] = CI[i] / patternEstimate[i];
      if(isfinite(errorFrac[i]) && (errorFrac[i] > maxErrorFrac)) {
        maxErrorFrac = errorFrac[i];
      }
    }
    if(0 == worldRank) {
      printf("triangles:   %llu +- %f (%f%% error)\n", patternEstimate[TRIANGLE], CI[TRIANGLE], errorFrac[TRIANGLE] * 100);
      printf("wedges:      %llu +- %f (%f%% error)\n", patternEstimate[WEDGE], CI[WEDGE], errorFrac[WEDGE] * 100);
      printf("4-clique:    %llu +- %f (%f%% error)\n", patternEstimate[CLIQUE4], CI[CLIQUE4], errorFrac[CLIQUE4] * 100);
      //printf("4-chordal:   %llu +- %f (%f%% error)\n", patternEstimate[CCYCLE4], CI[CCYCLE4], errorFrac[CCYCLE4] * 100);
      printf("4-cycle:     %llu +- %f (%f%% error)\n", patternEstimate[CYCLE4], CI[CYCLE4], errorFrac[CYCLE4] * 100);
      //printf("4-path:      %llu +- %f (%f%% error)\n", patternEstimate[PATH4], CI[PATH4], errorFrac[PATH4] * 100);
    }
  }
}

int main(int argc, char** argv) {
  int thread_level;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &thread_level);
  if(thread_level < MPI_THREAD_MULTIPLE) {
    fprintf(stderr, "Insufficient threading level\n");
    MPI_Abort(MPI::COMM_WORLD, 1);
  }
  omp_set_num_threads(omp_get_max_threads());
  worldSize = MPI::COMM_WORLD.Get_size();
  worldRank = MPI::COMM_WORLD.Get_rank();

  bool goodargs = (argc >= 2);
  string fname(argc >= 2 ? argv[1] : "");
  double p = (argc >= 3 ? strtod(argv[2], NULL) : 0.01);
  goodargs &= (p <= 1 && p > 0);
  double error_target = (argc >= 4 ? strtod(argv[3], NULL) : 0.01);
  goodargs &= (error_target <= 1 && error_target >= 0);
  double confidence = (argc >= 5 ? strtod(argv[4], NULL) : 0.95);
  goodargs &= (confidence <= 1 && confidence >= 0);
  if(!goodargs) {
    fprintf(stderr, "usage: ./count <fname> <sampling ratio [0,1]> <error target [0,1]> <confidence [0,1]>\n");
    return 1;
  }

  if(VERBOSE && worldRank == 0) {
    printf("processes: %d\n", worldSize);
    printf("threads:   %d\n", omp_get_max_threads());
  }
  Graph g(fname, worldSize, worldRank, VERBOSE);

  double t_start = MPI_Wtime();
  graphlet_decomposition(g, p, error_target, confidence);
  double t_end = MPI_Wtime();
  if(0 == worldRank) {
    printf("Wall time: %fs\n\n", (t_end-t_start));
  }
  MPI::Finalize();
  return 0;
}

