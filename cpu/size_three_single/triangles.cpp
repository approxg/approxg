#include <cstdio>
#include <vector>
#include <chrono>
#include <cstring>
#include <cstdlib>
using namespace std;

#include <omp.h>
#include "stats.h"
#include "mt19937.h"

vector< vector<unsigned> > graph;
vector< pair<unsigned, unsigned> > edges;
vector< double > randVals;
unsigned max_degree;

enum Pattern {
  TRIANGLE,
  WEDGE
};
const int multiplicity[] = { 3, 2 };
Pattern targetPattern = TRIANGLE;

/*
 * Every edge is input to this function in both directions
 * it returns true for one direction and false for the other
 */
inline bool dropHalf(int a, int b) {
  if(a < b) return (a % 2) != 0;
  else return (b % 2) == 0;
}

inline unsigned long nextVal(FILE* fd, char* buf, size_t buflen, char* &pos) {
  size_t endOffset = buf + buflen - pos;
  if(endOffset < 32 && !feof(fd)) {
    memcpy(buf, pos, endOffset);
    size_t read_bytes = fread(buf+endOffset, 1, buflen-endOffset, fd);
    if(read_bytes + endOffset < buflen) {
      buf[read_bytes + endOffset] = '\0';
    }
    pos = buf;
  }
  char* newpos;
  unsigned long val = strtoul(pos, &newpos, 10);
  pos = newpos;
  return val;
}


/*
 * Loads adjacency list file into graph and edges arrays
 */
void graphFromAdj(const char* fname) {
  mt19937 rand_gen;
  graph.clear();
  edges.clear();
  
  const size_t buflen = (1 << 20);
  FILE* fd = fopen(fname, "r");
  if(!fd) {
    fprintf(stderr, "Graph file opening failed\n");
    exit(1);
  }
  char* buf = new char[buflen];
  int bufferOffset = 0;

  char* pos = buf + buflen;

  unsigned N, src, deg, dest;
  size_t M;
  N = nextVal(fd, buf, buflen, pos);
  M = nextVal(fd, buf, buflen, pos);
  graph.resize(N);
  edges.reserve(M / 2);
  for(unsigned i = 0; i < N; i++) {
    src = nextVal(fd, buf, buflen, pos);
    deg = nextVal(fd, buf, buflen, pos);
    graph[src].reserve(deg);
    for(; deg; deg--) {
      dest = nextVal(fd, buf, buflen, pos);
      graph[src].push_back(dest);
      if(dropHalf(src, dest)) {
        edges.push_back(make_pair(src, dest));
        randVals.push_back(rand_gen.real());
      }
    }
  }
  delete[] buf;
  fclose(fd);
  printf("N:     %d\nM:     %li\n", N, M / 2);
}

/*
 * finds size of intersection of sets stored as sorted vectors
 */
inline void countTris(const vector<unsigned> &Nu, const vector<unsigned> &Nv) {
  unsigned long long triCount = 0;
  vector<unsigned>::const_iterator it1 = Nu.begin(), it2 = Nv.begin();
  while(it1 != Nu.end() && it2 != Nv.end()) {
    if(*it1 < *it2){
      it1++;
    } else if(*it1 > *it2){
      it2++;
    } else {
      it1++;
      it2++;
      triCount++;
    }
  }
  unsigned long long wedgeCount = Nv.size() + Nu.size() - 2*triCount - 2;
  stats::accumulate_online(TRIANGLE, triCount);
  stats::accumulate_online(WEDGE, wedgeCount);
}

inline void process_sample(double p, double p_previous) {
  int workers = omp_get_max_threads();
  vector<unsigned long long> threadPatterns(workers, 0);
  #pragma omp parallel for schedule(dynamic,64)
  for(long long e = 0; e < (long long)edges.size(); e++) {
    if(randVals[e] > p_previous && randVals[e] <= p) {
      unsigned v = edges[e].first, u = edges[e].second;
      countTris(graph[u], graph[v]);
    }
  }
}

/*
 * Results from each thread are collected at index tid of a vector
 * Sum of these accumulated values is the triangle count
 */
void graphlet_decomposition(double p_initial, double error_target, double confidence) {
  const long long m = edges.size();
  stats::reset_online(2);

  double p_previous = 0, p = p_initial;
  unsigned long long patternEstimate[2];
  double CI[2], errorFrac[2], maxErrorFrac;

  do {
    printf("p=%f\n", p);
    process_sample(p, p_previous);
    maxErrorFrac = 0.0;
    for(int i = 0; i < 2; i++) {
      patternEstimate[i] = stats::sum_online(i) / p / multiplicity[i];
      CI[i] = sqrt(stats::variance_online(i, m)) * stats::confidence_to_z(confidence) / multiplicity[i];
      errorFrac[i] = CI[i] / patternEstimate[i];
      if(errorFrac[i] > maxErrorFrac) maxErrorFrac = errorFrac[i];
    }
    printf("triangles: %llu +- %f (%f%% error)\n", patternEstimate[TRIANGLE], CI[TRIANGLE], errorFrac[TRIANGLE] * 100);
    printf("wedges:    %llu +- %f (%f%% error)\n", patternEstimate[WEDGE], CI[WEDGE], errorFrac[WEDGE] * 100);
    p_previous = p;
    double numerator = maxErrorFrac * maxErrorFrac * p_previous;
    double denominator = numerator + error_target * error_target * (1 - p_previous);
    p = numerator / denominator;
  } while(maxErrorFrac > error_target); 

}

int main(int argc, char** argv) {
  bool goodargs = (argc >= 2);
  const char* fname(argc >= 2 ? argv[1] : "");
  double p = (argc >= 3 ? strtod(argv[2], NULL) : 0.01);
  goodargs &= (p <= 1 && p >= 0);
  double error_target = (argc >= 4 ? strtod(argv[3], NULL) : 0.01);
  goodargs &= (error_target <= 1 && error_target >= 0);
  double confidence = (argc >= 5 ? strtod(argv[4], NULL) : 0.95);
  goodargs &= (confidence <= 1 && confidence >= 0);
  if(!goodargs) {
    fprintf(stderr, "usage: ./count <fname> <sampling ratio [0,1]> <error target [0,1]> <confidence [0,1]>\n");
    return 1;
  }
  
  printf("Opening adj file: %s\n", fname);
  graphFromAdj(fname);
  printf("worker threads:  %d\n", omp_get_max_threads());
  omp_set_num_threads(omp_get_max_threads());

  auto t_start = chrono::high_resolution_clock::now();
  graphlet_decomposition(p, error_target, confidence);
  auto t_end = std::chrono::high_resolution_clock::now();
  printf("Wall time: %fs\n\n", chrono::duration<double, ratio<1,1>>(t_end-t_start).count());
  return 0;
}
