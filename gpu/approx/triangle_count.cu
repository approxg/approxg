//=================================================================//
// CUDA BFS kernel
// Topological-Driven: one node per thread, thread_centric,
//      use atomicAdd instruction
//
// Reference: 
// T. Schank. Algorithmic Aspects of Triangle-Based Network Analysis
//=================================================================//
#include <cuda.h>
#include <stdint.h>
#include <stdio.h>
#include <algorithm>

#include "cudaGraph.h"
#include "mt19937.h"
#include "stats.h"

#define STACK_SZ 2048
#define SEED 123

bool COST_AWARE = true;

__device__ 
void quicksort(uint64_t array[], unsigned len)
{
    uint64_t left = 0, stack[STACK_SZ], pos = 0, seed = SEED;
    for ( ; ; )                                             /* outer loop */
    {
        for (; left+1 < len; len++)                  /* sort left to len-1 */
        {
            if (pos == STACK_SZ) len = stack[pos = 0];  /* stack overflow, reset */
            uint64_t pivot = array[left+seed%(len-left)];  /* pick random pivot */
            seed = seed*69069+1;                /* next pseudorandom number */
            stack[pos++] = len;                    /* sort right part later */
            for (unsigned right = left-1; ; )   /* inner loop: partitioning */
            {
                while (array[++right] < pivot);  /* look for greater element */
                while (pivot < array[--len]);    /* look for smaller element */
                if (right >= len) break;           /* partition point found? */
                uint64_t temp = array[right];
                array[right] = array[len];                  /* the only swap */
                array[len] = temp;
            }                            /* partitioned, continue left part */
        }
        if (pos == 0) break;                               /* stack empty? */
        left = len;                             /* left to right is sorted */
        len = stack[--pos];                      /* get next range to sort */
    }
}

__device__
unsigned get_intersect_cnt(uint64_t* setA, unsigned sizeA, uint64_t* setB, unsigned sizeB)
{
    unsigned ret=0;
    unsigned iter1=0, iter2=0;
    while (iter1<sizeA && iter2<sizeB) 
    {
        if (setA[iter1] < setB[iter2]) 
            iter1++;
        else if (setA[iter1] > setB[iter2]) 
            iter2++;
        else
        {
            ret++;
            iter1++;
            iter2++;
        }
    }

    return ret;
}

// sort the outgoing edges accoring to their ID order
__global__
void kernel_step1(cudaGraph graph) 
{
	uint64_t tid = blockIdx.x * blockDim.x + threadIdx.x;
    if (tid >= graph.vertex_cnt) return;
    uint64_t vid = tid + graph.vertex_offset;

    uint64_t start = graph.get_firstedge_index(vid);
    quicksort(graph.get_edge_ptr(start), graph.get_vertex_degree(vid));
}

// get neighbour set intersections of the vertices with edge links
__global__
void kernel_step2(uint32_t * edge_counts, cudaGraph graph, cudaGraph remote_graph, 
            float * rand_values, pair<float, float> lower_bound, pair<float, float> upper_bound)
{
    uint64_t tid = blockIdx.x * blockDim.x + threadIdx.x;
    if (tid >= graph.vertex_cnt) return;
    uint64_t vid = tid + graph.vertex_offset;

    uint64_t start = graph.get_firstedge_index(vid);
    uint64_t end = start + graph.get_vertex_degree(vid);
    for (uint64_t i=start; i<end; i++)
    {
        float r = rand_values[i - graph.edge_offset];
        uint64_t dest = graph.get_edge_dest(i);
        if (vid > dest) continue; // skip reverse edges
        if(dest > graph.vertex_offset && dest <= graph.vertex_offset + graph.vertex_cnt) { // dest is local
            if(r < lower_bound.first || r >= upper_bound.first) continue;
            uint64_t dest_start = graph.get_firstedge_index(dest);
            unsigned cnt = get_intersect_cnt(graph.get_edge_ptr(start),
                        graph.get_vertex_degree(vid), graph.get_edge_ptr(dest_start),
                        graph.get_vertex_degree(dest));
            edge_counts[i - graph.edge_offset] = cnt;
        } else { // dest is remote
            if(r <= lower_bound.second || r > upper_bound.second) continue;
            uint64_t dest_start = remote_graph.get_firstedge_index(dest);
            unsigned cnt = get_intersect_cnt(graph.get_edge_ptr(start),
                        graph.get_vertex_degree(vid), remote_graph.get_edge_ptr(dest_start),
                        remote_graph.get_vertex_degree(dest));
            edge_counts[i - graph.edge_offset] = cnt;
        }
    }
}

inline unsigned int blocks(uint64_t threads) {
    return (threads + 31) / 32;
}

pair<float, float> pslr_calc(float p) {
    if(!COST_AWARE) return make_pair(p, p);
    if(p <= 0.5) {
        return make_pair(p * 2.0, 0.0);
    } else {
        return make_pair(1.0, p * 2.0 - 1.0);
    }
}

unsigned cuda_triangle_count(uint64_t * vertexlist,
        uint64_t * edgelist, uint32_t * vproplist,
        uint64_t vertex_cnt, uint64_t edge_cnt)
{
    int nDevices;
    cudaGetDeviceCount(&nDevices);
    if(nDevices < 2) {
        printf("Error, not enough GPUs\n");
        exit(1);
    }

    float h2d_copy_time1 = 0, h2d_copy_time2 = 0; // host to device data transfer time
    float kernel_time1 = 0, kernel_time2 = 0;   // kernel execution time

    uint64_t vertex_offset = std::lower_bound(vertexlist, vertexlist + vertex_cnt, edge_cnt / 2) - vertexlist;
    uint64_t edge_offset = vertexlist[vertex_offset];

    uint64_t tcount = 0;
    uint32_t * device_vpl1 = 0, * device_vpl2 = 0;
    //random array
    float * rand_values = new float[edge_cnt];
    mt19937 local_rand;
    for(uint64_t i = 0; i < edge_cnt; i++) {
        rand_values[i] = local_rand.real();
    }
    float * device_rand_values1 = 0, * device_rand_values2 = 0;
    //uint32_t * edge_counts = new uint32_t[edge_cnt];
    uint32_t * edge_counts = 0;

    cudaHostAlloc(&edge_counts, edge_cnt*sizeof(uint32_t), cudaHostAllocMapped);
    // malloc of gpu side
    cudaSetDevice(0);
    cudaErrCheck( cudaMalloc((void**)&device_rand_values1, edge_offset*sizeof(float)) );
    cudaSetDevice(1);
    cudaErrCheck( cudaMalloc((void**)&device_rand_values2, (edge_cnt - edge_offset)*sizeof(float)) );

    cudaEvent_t start_event1, stop_event1;
    cudaSetDevice(0);
    cudaDeviceEnablePeerAccess(1, 0);
    cudaErrCheck( cudaEventCreate(&start_event1) );
    cudaErrCheck( cudaEventCreate(&stop_event1) );
    cudaEvent_t start_event2, stop_event2;
    cudaSetDevice(1);
    cudaDeviceEnablePeerAccess(0, 0);
    cudaErrCheck( cudaEventCreate(&start_event2) );
    cudaErrCheck( cudaEventCreate(&stop_event2) );
    
    // prepare graph struct
    //  one for host side, one for device side
    cudaGraph h_graph1, d_graph1;
    cudaGraph h_graph2, d_graph2;
    
    // here copy only the pointers
    h_graph1.read(vertexlist, edgelist, vertex_offset, edge_offset, 0, 0);
    h_graph2.read(vertexlist, edgelist, vertex_cnt, edge_cnt, vertex_offset, edge_offset);

    // copy graph data to device
    cudaSetDevice(0);
    cudaEventRecord(start_event1, 0);
    h_graph1.cudaGraphCopy(&d_graph1);
    cudaErrCheck( cudaMemcpy(device_rand_values1, rand_values, edge_offset*sizeof(float), 
                cudaMemcpyHostToDevice) );
    cudaEventRecord(stop_event1, 0);
    cudaSetDevice(1);
    cudaEventRecord(start_event2, 0);
    h_graph2.cudaGraphCopy(&d_graph2);
    cudaErrCheck( cudaMemcpy(device_rand_values2, rand_values + edge_offset, 
                (edge_cnt - edge_offset)*sizeof(float), cudaMemcpyHostToDevice) );
    cudaEventRecord(stop_event2, 0);

    cudaEventSynchronize(stop_event1);
    cudaEventElapsedTime(&h2d_copy_time1, start_event1, stop_event1);
    cudaEventSynchronize(stop_event2);
    cudaEventElapsedTime(&h2d_copy_time2, start_event2, stop_event2);
    
    cudaSetDevice(0);
    cudaEventRecord(start_event1, 0);
    kernel_step1<<<blocks(vertex_offset), 32>>>(d_graph1);
    cudaSetDevice(1);
    cudaEventRecord(start_event2, 0);
    kernel_step1<<<blocks(vertex_cnt - vertex_offset), 32>>>(d_graph2);

    cudaSetDevice(0);
    cudaErrCheck( cudaDeviceSynchronize() );
    cudaSetDevice(1);
    cudaErrCheck( cudaDeviceSynchronize() );

    stats::reset_online(1);

    /*****/
    float p_previous = 0, p = 0.0001;
    uint64_t patternEstimate;
    double CI, errorFrac;
    double confidence = 0.95;
    double errorTarget = 0.01;
    
    do {
        pair<float, float> pp_previous = pslr_calc(p_previous);
        pair<float, float> pp = pslr_calc(p);
        printf("p: %f\n", p);
        cudaSetDevice(0);
        kernel_step2<<<blocks(vertex_offset), 32, 0>>>(edge_counts, d_graph1, d_graph2, 
                    device_rand_values1, pp_previous, pp);
        cudaSetDevice(1);
        kernel_step2<<<blocks(vertex_cnt - vertex_offset), 32, 0>>>(edge_counts + edge_offset, d_graph2, d_graph1, 
                    device_rand_values2, pp_previous, pp);

        cudaSetDevice(0);
        cudaErrCheck( cudaDeviceSynchronize() );
        cudaSetDevice(1);
        cudaErrCheck( cudaDeviceSynchronize() );
    
        //#pragma omp parallel for
        uint64_t vnum = 0;
        for(uint64_t i = 0; i < edge_cnt; i++) {
            if(i >= vertexlist[vnum + 1]) vnum++;
            if(vnum >= edgelist[i]) continue;
            uint64_t dest = edgelist[i];
            if(vnum < vertex_offset && dest < vertex_offset) {
                if(pp_previous.first < rand_values[i] && rand_values[i] <= pp.first) {
                    stats::accumulate_online(0, edge_counts[i]);
                }
            } else if(vnum >= vertex_offset && dest >= vertex_offset) {
                if(pp_previous.first < rand_values[i] && rand_values[i] <= pp.first) {
                    stats::accumulate_online(0, edge_counts[i]);
                }
            } else {
                if(pp_previous.second < rand_values[i] && rand_values[i] <= pp.second) {
                    stats::accumulate_online(0, edge_counts[i]);
                }
            }
            /*if(vnum < vertex_offset && dest > h_graph1.vertex_offset && 
                  dest <= h_graph1.vertex_offset + h_graph1.vertex_cnt) {
                if(pp_previous.first < rand_values[i] && rand_values[i] <= pp.first) {
                    stats::accumulate_online(0, edge_counts[i]);
                }
            } else if(vnum >= vertex_offset && dest > h_graph2.vertex_offset && 
                  dest <= h_graph2.vertex_offset + h_graph1.vertex_cnt) {
                if(pp_previous.first < rand_values[i] && rand_values[i] <= pp.first) {
                    stats::accumulate_online(0, edge_counts[i]);
                }
            } else {
                if(pp_previous.second < rand_values[i] && rand_values[i] <= pp.second) {
                    stats::accumulate_online(0, edge_counts[i]);
                }
            }*/
            /*if((vnum < vertex_offset) == (edgelist[i] < edge_offset)) { //local
                if(pp_previous.first < rand_values[i] && rand_values[i] <= pp.first) {
                    stats::accumulate_online(0, edge_counts[i]);
                }
            } else { //remote
                if(pp_previous.second < rand_values[i] && rand_values[i] <= pp.second) {
                    stats::accumulate_online(0, edge_counts[i]);
                }
            }*/
        }
        printf("sampled: %llu\n", stats::count_online(0));
        p_previous = p;
        patternEstimate = stats::sum_online(0) / p / 3;
        CI = sqrt(stats::variance_online(0, edge_cnt / 2)) * stats::confidence_to_z(confidence) / 3;
        errorFrac = CI / patternEstimate;
        printf("triangles: %lu +- %f (%f%% error)\n", patternEstimate, CI, errorFrac * 100);
        double numerator = errorFrac * errorFrac * p_previous;
        double denominator = numerator + errorTarget * errorTarget * (1 - p_previous);
        p = numerator / denominator;
        if(p - p_previous < 0.0001) {
            p = p_previous + 0.0001;
        }
    } while(errorFrac > errorTarget);
    tcount = patternEstimate;
    /*****/

    cudaSetDevice(0);
    cudaEventRecord(stop_event1, 0);
    cudaSetDevice(1);
    cudaEventRecord(start_event2, 0);

    cudaEventSynchronize(stop_event1);
    cudaEventElapsedTime(&kernel_time1, start_event1, stop_event1);
    cudaEventSynchronize(stop_event2);
    cudaEventElapsedTime(&kernel_time2, start_event2, stop_event2);

#ifndef ENABLE_VERIFY
    printf("== host->device copy time: %f ms\n", std::max(h2d_copy_time1, h2d_copy_time2));
    printf("== kernel time: %f ms\n", std::max(kernel_time1, kernel_time2));
#endif

    cudaEventDestroy(start_event1);
    cudaEventDestroy(stop_event1);
    cudaEventDestroy(start_event2);
    cudaEventDestroy(stop_event2);

    // free graph struct on device side
    d_graph1.cudaGraphFree();
    d_graph2.cudaGraphFree();

    cudaErrCheck( cudaFree(device_vpl1) );
    cudaErrCheck( cudaFree(device_vpl2) );
    cudaErrCheck( cudaFreeHost(edge_counts) );
    cudaErrCheck( cudaFree(device_rand_values1) );
    cudaErrCheck( cudaFree(device_rand_values2) );
    delete[] rand_values;

    return tcount;
}
