//=================================================================//
// CUDA BFS kernel
// Topological-Driven: one node per thread, thread_centric,
//      use atomicAdd instruction
//
// Reference: 
// T. Schank. Algorithmic Aspects of Triangle-Based Network Analysis
//=================================================================//
#include <cuda.h>
#include <stdint.h>
#include <stdio.h>
#include <algorithm>

#include "cudaGraph.h"
#include "mt19937.h"

#define STACK_SZ 2048
#define SEED 123

__device__ 
void quicksort(uint64_t array[], unsigned len)
{
    uint64_t left = 0, stack[STACK_SZ], pos = 0, seed = SEED;
    for ( ; ; )                                             /* outer loop */
    {
        for (; left+1 < len; len++)                  /* sort left to len-1 */
        {
            if (pos == STACK_SZ) len = stack[pos = 0];  /* stack overflow, reset */
            uint64_t pivot = array[left+seed%(len-left)];  /* pick random pivot */
            seed = seed*69069+1;                /* next pseudorandom number */
            stack[pos++] = len;                    /* sort right part later */
            for (unsigned right = left-1; ; )   /* inner loop: partitioning */
            {
                while (array[++right] < pivot);  /* look for greater element */
                while (pivot < array[--len]);    /* look for smaller element */
                if (right >= len) break;           /* partition point found? */
                uint64_t temp = array[right];
                array[right] = array[len];                  /* the only swap */
                array[len] = temp;
            }                            /* partitioned, continue left part */
        }
        if (pos == 0) break;                               /* stack empty? */
        left = len;                             /* left to right is sorted */
        len = stack[--pos];                      /* get next range to sort */
    }
}

__device__
unsigned get_intersect_cnt(uint64_t* setA, unsigned sizeA, uint64_t* setB, unsigned sizeB)
{
    unsigned ret=0;
    unsigned iter1=0, iter2=0;
    while (iter1<sizeA && iter2<sizeB) 
    {
        if (setA[iter1] < setB[iter2]) 
            iter1++;
        else if (setA[iter1] > setB[iter2]) 
            iter2++;
        else
        {
            ret++;
            iter1++;
            iter2++;
        }
    }

    return ret;
}

// sort the outgoing edges accoring to their ID order
__global__
void kernel_step1(uint32_t * vplist, cudaGraph graph) 
{
	uint64_t tid = blockIdx.x * blockDim.x + threadIdx.x;
    if (tid >= graph.vertex_cnt) return;
    uint64_t vid = tid + graph.vertex_offset;

    uint64_t start = graph.get_firstedge_index(vid);
    quicksort(graph.get_edge_ptr(start), graph.get_vertex_degree(vid));
}

// get neighbour set intersections of the vertices with edge links
__global__
void kernel_step2(uint32_t * vplist, cudaGraph graph, cudaGraph remote_graph)
{
    uint64_t tid = blockIdx.x * blockDim.x + threadIdx.x;
    if (tid >= graph.vertex_cnt) return;
    uint64_t vid = tid + graph.vertex_offset;

    vplist[tid] = 0;
    uint64_t start = graph.get_firstedge_index(vid);
    uint64_t end = start + graph.get_vertex_degree(vid);
    for (uint64_t i=start; i<end; i++)
    {
        uint64_t dest = graph.get_edge_dest(i);
        if (vid > dest) continue; // skip reverse edges
        unsigned cnt;
        if(dest >= graph.vertex_offset && dest < graph.vertex_offset + graph.vertex_cnt) { // dest is local
            uint64_t dest_start = graph.get_firstedge_index(dest);
            cnt = get_intersect_cnt(graph.get_edge_ptr(start),
                        graph.get_vertex_degree(vid), graph.get_edge_ptr(dest_start),
                        graph.get_vertex_degree(dest));
        } else { // dest is remote
            uint64_t dest_start = remote_graph.get_firstedge_index(dest);
            cnt = get_intersect_cnt(graph.get_edge_ptr(start),
                        graph.get_vertex_degree(vid), remote_graph.get_edge_ptr(dest_start),
                        remote_graph.get_vertex_degree(dest));
        }
        vplist[tid] += cnt;
    }
}

// reduction to get the total count
__global__
void kernel_step3(uint32_t * vplist, uint64_t vertex_cnt, unsigned long long int * d_tcount) 
{
    __shared__ unsigned t_local[32];
	uint64_t tid = blockIdx.x * blockDim.x + threadIdx.x;
    if(tid < vertex_cnt)
        t_local[threadIdx.x] = vplist[tid];
    else
        t_local[threadIdx.x] = 0;
    if(threadIdx.x < 16) t_local[threadIdx.x] += t_local[threadIdx.x + 16];
    if(threadIdx.x <  8) t_local[threadIdx.x] += t_local[threadIdx.x +  8];
    if(threadIdx.x <  4) t_local[threadIdx.x] += t_local[threadIdx.x +  4];
    if(threadIdx.x <  2) t_local[threadIdx.x] += t_local[threadIdx.x +  2];
    if(threadIdx.x <  1) t_local[threadIdx.x] += t_local[threadIdx.x +  1];
    if(threadIdx.x == 0) 
        atomicAdd(d_tcount, t_local[0]);
}

inline unsigned int blocks(uint64_t threads) {
    return (threads + 31) / 32;
}

uint64_t cuda_triangle_count(uint64_t * vertexlist,
        uint64_t * edgelist, uint32_t * vproplist,
        uint64_t vertex_cnt, uint64_t edge_cnt)
{
    int nDevices;
    cudaGetDeviceCount(&nDevices);
    if(nDevices < 2) {
        printf("Error, not enough GPUs\n");
        exit(1);
    }

    uint64_t tcount = 0;
    uint32_t * device_vpl1 = 0, * device_vpl2 = 0;
    unsigned long long int * device_tcount1 = 0, * device_tcount2 = 0;

    float h2d_copy_time = 0; // host to device data transfer time
    float d2h_copy_time = 0; // device to host data transfer time
    float kernel_time = 0;   // kernel execution time

    uint64_t vertex_offset = std::lower_bound(vertexlist, vertexlist + vertex_cnt, edge_cnt / 2) - vertexlist;
    uint64_t edge_offset = vertexlist[vertex_offset];

    //random array
    float * rand_values = new float[edge_cnt];
    mt19937 local_rand;
    for(uint64_t i = 0; i < edge_cnt; i++) {
        rand_values[i] = local_rand.real();
    }
    float * device_rand_values1 = 0, * device_rand_values2 = 0;

    int device;
    cudaGetDevice(&device);
    cudaDeviceProp devProp;
    cudaGetDeviceProperties(&devProp,device);

    // malloc of gpu side
    cudaSetDevice(0);
    cudaErrCheck( cudaMalloc((void**)&device_vpl1, vertex_offset*sizeof(uint32_t)) );
    cudaErrCheck( cudaMalloc((void**)&device_rand_values1, edge_offset*sizeof(float)) );
    cudaErrCheck( cudaMalloc((void**)&device_tcount1, sizeof(uint64_t)) );
    cudaSetDevice(1);
    cudaErrCheck( cudaMalloc((void**)&device_vpl2, (vertex_cnt - vertex_offset)*sizeof(uint32_t)) );
    cudaErrCheck( cudaMalloc((void**)&device_rand_values2, (edge_cnt - edge_offset)*sizeof(float)) );
    cudaErrCheck( cudaMalloc((void**)&device_tcount2, sizeof(uint64_t)) );

    cudaEvent_t start_event, stop_event;
    cudaSetDevice(0);
    cudaErrCheck( cudaEventCreate(&start_event) );
    cudaErrCheck( cudaEventCreate(&stop_event) );
    
    // prepare graph struct
    //  one for host side, one for device side
    cudaGraph h_graph1, d_graph1;
    cudaGraph h_graph2, d_graph2;
    
    // here copy only the pointers
    h_graph1.read(vertexlist, edgelist, vertex_offset, edge_offset, 0, 0);
    h_graph2.read(vertexlist, edgelist, vertex_cnt, edge_cnt, vertex_offset, edge_offset);

    // memcpy from host to device
    cudaSetDevice(0);
    cudaEventRecord(start_event, 0);
   
    // copy graph data to device
    uint64_t zeronum = 0;
    cudaSetDevice(0);
    h_graph1.cudaGraphCopy(&d_graph1);
    cudaErrCheck( cudaMemcpy(device_rand_values1, rand_values, edge_offset*sizeof(float), 
                cudaMemcpyHostToDevice) );
    cudaErrCheck( cudaMemcpy(device_tcount1, &zeronum, sizeof(uint64_t), 
                cudaMemcpyHostToDevice) );
    cudaEventRecord(stop_event, 0);
    cudaEventSynchronize(stop_event);
    cudaEventElapsedTime(&h2d_copy_time, start_event, stop_event);
    cudaSetDevice(1);
    h_graph2.cudaGraphCopy(&d_graph2);
    cudaErrCheck( cudaMemcpy(device_rand_values2, rand_values + edge_offset, 
                (edge_cnt - edge_offset)*sizeof(float), cudaMemcpyHostToDevice) );
    cudaErrCheck( cudaMemcpy(device_tcount2, &zeronum, sizeof(uint64_t), 
                cudaMemcpyHostToDevice) );
    
    cudaSetDevice(0);
    cudaEventRecord(start_event, 0);
    kernel_step1<<<blocks(vertex_offset), 32>>>(device_vpl1, d_graph1);
    cudaDeviceEnablePeerAccess(1, 0);
    cudaSetDevice(1);
    kernel_step1<<<blocks(vertex_cnt - vertex_offset), 32>>>(device_vpl2, d_graph2);
    cudaDeviceEnablePeerAccess(0, 0);

    cudaDeviceSynchronize();

    cudaSetDevice(0);
    kernel_step2<<<blocks(vertex_offset), 32>>>(device_vpl1, d_graph1, d_graph2);
    kernel_step3<<<blocks(vertex_offset), 32>>>(device_vpl1, vertex_offset, device_tcount1);
    cudaSetDevice(1);
    kernel_step2<<<blocks(vertex_cnt - vertex_offset), 32>>>(device_vpl2, d_graph2, d_graph1);
    kernel_step3<<<blocks(vertex_cnt - vertex_offset), 32>>>(device_vpl2, vertex_cnt - vertex_offset, device_tcount2);

    uint64_t temp;
    cudaErrCheck( cudaMemcpy(&tcount, device_tcount1, sizeof(uint64_t), cudaMemcpyDeviceToHost) );
    cudaErrCheck( cudaMemcpy(&temp, device_tcount2, sizeof(uint64_t), cudaMemcpyDeviceToHost) );
    tcount = (tcount + temp) / 3;

    cudaSetDevice(0);
    cudaEventRecord(stop_event, 0);
    cudaEventSynchronize(stop_event);
    cudaEventElapsedTime(&kernel_time, start_event, stop_event);

    cudaEventRecord(start_event, 0);
    cudaErrCheck( cudaMemcpy(vproplist, device_vpl1, vertex_offset*sizeof(uint32_t), 
                cudaMemcpyDeviceToHost) );
    cudaEventRecord(stop_event, 0);
    cudaEventSynchronize(stop_event);
    cudaEventElapsedTime(&d2h_copy_time, start_event, stop_event);

    cudaErrCheck( cudaMemcpy(vproplist + vertex_offset, device_vpl2, (vertex_cnt - vertex_offset)*sizeof(uint32_t), 
                cudaMemcpyDeviceToHost) );
    
    
#ifndef ENABLE_VERIFY
    printf("== host->device copy time: %f ms\n", h2d_copy_time);
    printf("== device->host copy time: %f ms\n", d2h_copy_time);
    printf("== kernel time: %f ms\n", kernel_time);
#endif

    cudaEventDestroy(start_event);
    cudaEventDestroy(stop_event);

    // free graph struct on device side
    d_graph1.cudaGraphFree();
    d_graph2.cudaGraphFree();

    cudaErrCheck( cudaFree(device_vpl1) );
    cudaErrCheck( cudaFree(device_vpl2) );
    cudaErrCheck( cudaFree(device_tcount1) );
    cudaErrCheck( cudaFree(device_tcount2) );
    cudaErrCheck( cudaFree(device_rand_values1) );
    cudaErrCheck( cudaFree(device_rand_values2) );
    delete[] rand_values;

    return tcount;
}
