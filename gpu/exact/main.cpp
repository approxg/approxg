//====== Graph Benchmark Suites ======//
//

#include <vector>
#include <string>
#include <fstream>
#include <chrono>

#include "openG.h"
#include "utility.h"

using namespace std;

extern unsigned cuda_triangle_count(
        uint64_t * vertexlist, 
        uint64_t * edgelist, uint32_t * vproplist,
        uint64_t vertex_cnt, uint64_t edge_cnt);

class vertex_property
{
public:
    vertex_property():value(0){}
    vertex_property(uint64_t x):value(x){}

    uint64_t value;
};
class edge_property
{
public:
    edge_property():value(0){}
    edge_property(uint64_t x):value(x){}

    uint64_t value;
};

typedef openG::extGraph<vertex_property, edge_property> graph_t;
typedef graph_t::vertex_iterator    vertex_iterator;
typedef graph_t::edge_iterator      edge_iterator;

//==============================================================//
//==============================================================//

void output(vector<uint32_t> & vproplist)
{
    cout<<"Triangle Count Results:\n";
    for(size_t i=0;i<vproplist.size();i++)
    {
        cout<<"== vertex "<<i<<": count "<<vproplist[i]<<endl;
    }
}

//==============================================================//
int main(int argc, char * argv[])
{
		bool goodargs = (argc >= 2);
		string fname(argc >= 2 ? argv[1] : "");
		if(!goodargs) {
				cerr << "usage: ./triangle_count <fname>\n";
				return 1;
		}
    //graphBIG::print();
    cout<<"Benchmark: GPU Triangle Count\n";

    cout<<"loading data... \n";

    size_t vertex_num, edge_num;
    vector<uint64_t> vertexlist, edgelist;
    load_ADJ_graph(fname, vertex_num, edge_num, vertexlist, edgelist);
    cout << "Graph loaded\n";

    cout<<"== "<<vertex_num<<" vertices  "<<edge_num<<" edges\n";
    
    //================================================//
    vector<uint32_t> vproplist(vertex_num, 0);
    //================================================//
    
    uint64_t tcount;
    auto t_start = chrono::high_resolution_clock::now();
    //================================================//
    // call CUDA function 
    tcount = cuda_triangle_count(&(vertexlist[0]), 
            &(edgelist[0]), &(vproplist[0]), 
            vertexlist.size()-1, edgelist.size());
    //================================================//
    auto t_end = std::chrono::high_resolution_clock::now();
    

    cout<<"\nGPU Triangle Count finish: \n";
    cout<<"== "<<vertex_num<<" vertices  "<<edge_num<<" edges\n";
    cout<<"== total triangle count: "<<tcount<<"\n";
    cout << "time: " << chrono::duration<double, ratio<1,1>>(t_end-t_start).count() << " s\n";

    cout<<"==================================================================\n";
    return 0;
}  // end main

