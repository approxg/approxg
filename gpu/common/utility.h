#pragma once

#include <cstring>
#include <string>
#include <vector>

inline uint64_t nextVal(FILE* fd, char* buf, size_t buflen, char* &pos) {
  size_t endOffset = buf + buflen - pos;
  if(endOffset < 32 && !feof(fd)) {
    memcpy(buf, pos, endOffset);
    size_t read_bytes = fread(buf+endOffset, 1, buflen-endOffset, fd);
    if(read_bytes + endOffset < buflen) {
      buf[read_bytes + endOffset] = '\0';
    }
    pos = buf;
  }
  char* newpos;
  unsigned long val = strtoul(pos, &newpos, 10);
  pos = newpos;
  return val;
}

/*
 * Loads adjacency list file into graph and edges arrays
 */
void load_ADJ_graph(const std::string& fname,
      uint64_t & vertex_num,
      uint64_t & edge_num,
      std::vector<uint64_t> & vertexlist,
      std::vector<uint64_t> & edgelist)
{
  vertexlist.resize(0);
  edgelist.resize(0);
  const size_t buflen = (1 << 20);
  FILE* fd = fopen(fname.c_str(), "r");
  if(!fd) {
    fprintf(stderr, "Graph file opening failed\n");
    exit(1);
  }
  char* buf = new char[buflen];
  char* pos = buf + buflen;

  uint64_t deg, dest;
  vertex_num = nextVal(fd, buf, buflen, pos);
  edge_num = nextVal(fd, buf, buflen, pos);
  vertexlist.reserve(vertex_num + 1);
  edgelist.reserve(edge_num);
  vertexlist.push_back(0);
  for(unsigned i = 0; i < vertex_num; i++) {
    nextVal(fd, buf, buflen, pos);
    deg = nextVal(fd, buf, buflen, pos);
    vertexlist.push_back(vertexlist.back() + deg);
    for(; deg; deg--) {
      dest = nextVal(fd, buf, buflen, pos);
      edgelist.push_back(dest);
    } 
  }
  delete[] buf;
  fclose(fd);
  //printf("N:     %d\nM:     %li\n", N, M / 2);
}

