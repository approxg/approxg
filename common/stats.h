#pragma once
#include <cmath>
#include <cstdio>
#include <vector>
#include <omp.h>
#ifdef MPI_VERSION
  #include <mpi.h>
#endif

class stats {
private:
  static std::vector< std::vector<unsigned long long> > on_n;
  static std::vector< std::vector<unsigned long long> > on_sum;
  static std::vector< std::vector<double> > on_mean, on_M2;
  
  // http://www.johndcook.com/blog/cpp_phi/
  static double phi(double x) {
      // constants
      double a1 =  0.254829592;
      double a2 = -0.284496736;
      double a3 =  1.421413741;
      double a4 = -1.453152027;
      double a5 =  1.061405429;
      double p  =  0.3275911;

      // Save the sign of x
      int sign = 1;
      if (x < 0)
          sign = -1;
      x = fabs(x)/sqrt(2.0);

      // A&S formula 7.1.26
      double t = 1.0/(1.0 + p*x);
      double y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*exp(-x*x);

      return 0.5*(1.0 + sign*y);
  }

  // binary search for near-match
  static double inv_phi(double p) {
    double Z_EPSILON = 0.000001;
    double min = 0, max = 6;
    do {
      double guess = min + (max - min) / 2;
      double result = phi(guess);
      if(result > p) max = guess;
      if(result < p) min = guess;
    } while(min + Z_EPSILON < max);
    return min + (max - min) / 2;
  }

public:
  static inline double confidence_to_z(double c) {
    return inv_phi(1 - ( (1 - c) / 2));
  }

  static void reset_online(unsigned categories) {
    #pragma omp critical
    {
      on_n.assign(omp_get_max_threads(), std::vector<unsigned long long>(categories));
      on_sum.assign(omp_get_max_threads(), std::vector<unsigned long long>(categories));
      on_mean.assign(omp_get_max_threads(), std::vector<double>(categories));
      on_M2.assign(omp_get_max_threads(), std::vector<double>(categories));
    }
  }

  static void accumulate_online(unsigned category, unsigned long long x) {
    while(on_sum[omp_get_thread_num()].size() <= category) {
      on_n[omp_get_thread_num()].push_back(0);
      on_sum[omp_get_thread_num()].push_back(0);
      on_mean[omp_get_thread_num()].push_back(0);
      on_M2[omp_get_thread_num()].push_back(0);
    }
    on_sum[omp_get_thread_num()][category] += x;
    on_n[omp_get_thread_num()][category]++;
    double delta = x - on_mean[omp_get_thread_num()][category];
    on_mean[omp_get_thread_num()][category] += delta / on_n[omp_get_thread_num()][category];
    on_M2[omp_get_thread_num()][category] += delta * (x - on_mean[omp_get_thread_num()][category]);
  }

  static unsigned long long sum_online(unsigned category) {
    unsigned long long sum = 0;
    #pragma omp critical
    {
      for(int i = 0; i < omp_get_max_threads(); i++) {
        sum += on_sum[i][category];
      }
      #ifdef MPI_VERSION
        MPI_Allreduce(MPI_IN_PLACE, &sum, 1, MPI_UNSIGNED_LONG, MPI_SUM, MPI::COMM_WORLD);
      #endif
    }
    return sum;
  }

  static unsigned long long count_online(unsigned category) {
    unsigned long long count = 0;
    #pragma omp critical
    {
      for(int i = 0; i < omp_get_max_threads(); i++) {
        count += on_n[i][category];
      }
      #ifdef MPI_VERSION
        MPI_Allreduce(MPI_IN_PLACE, &count, 1, MPI_UNSIGNED_LONG, MPI_SUM, MPI::COMM_WORLD);
      #endif
    }
    return count;
  }

  static double variance_online(unsigned category, size_t N) {
    double sumVariance = 0;
    #pragma omp critical
    {
      for(int i = 1; i < omp_get_max_threads(); i++) {
        double delta = on_mean[0][category] - on_mean[i][category];
        on_M2[0][category] = on_M2[0][category] + on_M2[i][category] + delta * delta * on_n[0][category] * on_n[i][category] / (on_n[0][category] + on_n[i][category]);
        on_M2[i][category] = 0;
        on_mean[0][category] = (on_mean[0][category] * on_n[0][category] + on_mean[i][category] + on_n[i][category]) / (on_n[0][category] + on_n[i][category]);
        on_mean[i][category] = 0;
        on_n[0][category] += on_n[i][category];
        on_n[i][category] = 0;
      }
      #ifdef MPI_VERSION
        int worldSize = MPI::COMM_WORLD.Get_size();
        vector<double> inter_M2(worldSize);
        vector<double> inter_mean(worldSize);
        vector<unsigned long long> inter_n(worldSize);
        MPI_Allgather(&(on_M2[0][category]), 1, MPI_DOUBLE, &(inter_M2[0]), 1, MPI_DOUBLE, MPI::COMM_WORLD);
        MPI_Allgather(&(on_mean[0][category]), 1, MPI_DOUBLE, &(inter_mean[0]), 1, MPI_DOUBLE, MPI::COMM_WORLD);
        MPI_Allgather(&(on_n[0][category]), 1, MPI_UNSIGNED_LONG, &(inter_n[0]), 1, MPI_UNSIGNED_LONG, MPI::COMM_WORLD);
        for(int i = 1; i < worldSize; i++) {
          double delta = inter_mean[0] - inter_mean[i];
          inter_M2[0] = inter_M2[0] + inter_M2[i] + delta * delta * inter_n[0] * inter_n[i] / (inter_n[0] + inter_n[i]);
          inter_M2[i] = 0;
          inter_mean[0] = (inter_mean[0] * inter_n[0] + inter_mean[i] + inter_n[i]) / (inter_n[0] + inter_n[i]);
          inter_mean[i] = 0;
          inter_n[0] += inter_n[i];
          inter_n[i] = 0;
        }
        sumVariance = N * (N - inter_n[0]) * (inter_M2[0] / (inter_n[0] - 1))  / inter_n[0];
      #else
        sumVariance = N * (N - on_n[0][category]) * (on_M2[0][category] / (on_n[0][category] - 1))  / on_n[0][category];
      #endif
    }
    return sumVariance;
  }

};

std::vector< std::vector<unsigned long long> > stats::on_n = std::vector< std::vector<unsigned long long> >(0);
std::vector< std::vector<unsigned long long> > stats::on_sum = std::vector< std::vector<unsigned long long> >(0);
std::vector< std::vector<double> > stats::on_mean = std::vector< std::vector<double> >(0);
std::vector< std::vector<double> > stats::on_M2 = std::vector< std::vector<double> >(0);
