# README #

ApproxG is a large graph processing framework focused on graphlet counting. Versions are available targeting different parallel processing paradigms including single-machine OpenMP, multi-machine hybrid OpenMP/MPI, and 2-GPU.

* Each application counts graphlets consisting of 3 or 4 vertices (i.e. triangles, rectangles, etc.)
* Version 1.0

## Setup ##
#### Datasets ####
* scripts/getLAW and scripts/getSNAP
* requires bash, python, g++, make
* from LAW (http://law.di.unimi.it/datasets.php) : ./getLAW <dataset (i.e. uk-2002)>
* from SNAP (https://snap.stanford.edu/data/index.html): ./getSNAP <download link (i.e. https://snap.stanford.edu/data/bigdata/communities/com-orkut.ungraph.txt.gz)> <name (i.e. orkut)>
* downloaded graphs stored to ~/data/<name>
#### Single-Machine OpenMP ####
* cpu/size_three_single and cpu/size_four_single
* requires g++, make
* make && ./<binary> <graph file (i.e. ~/data/<name>)> <initial sampling ratio> <error target> <confidence>
#### Multi-Machine Hybrid OpenMP/MPI ####
* cpu/three and cpu/four
* requires MPI 2.0-compatible, c++98-compliant compiler
* scripts assume slurm cluster manager
* make && ./run <graph file (i.e. ~/data/<name>)> <initial sampling ratio> <error target> <confidence>
* results stored to ~/results/<pattern size>/
#### 2-GPU ####
* gpu/exact and gpu/approx
* requires NVIDIA CUDA 7.5 or higher and g++
* assumes a system containing only 2 identical GPUs
* make && ./triangle_count <graph file (i.e. ~/data/<name>)> <initial sampling ratio> <error target> <confidence>